//
//  SearchViewModel.swift
//  Empresas
//
//  Created by Marcos Jr on 30/05/21.
//

import UIKit
import Foundation

class SearchViewModel: SearchDelegate {
    
    var model: HeadersModel!
    var enterprisesModel: EnterprisesModel!
    var tableView: UITableView!
    let endpoint = Endpoint()
    
    init(model: HeadersModel) {
        self.model = model
    }
    
    func getCompanies(param: String, completionHandler: @escaping (HTTPURLResponse, Data?) -> Void) {
        guard let url = URL(string: endpoint.getCompanies + param) else { return }
        let request = generateHeader(url: url)
        
        do {
            URLSession.shared.dataTask(with: request) { (data, response, error) in
                if let error = error {
                    print("Failed to get: ", error)
                    return
                }
                
                guard let httpResponse = response as? HTTPURLResponse else { return }
                completionHandler(httpResponse, data)
            
            }.resume()
        }
    }
    
    func generateHeader(url: URL) -> URLRequest {
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.addValue(model.contentType ?? "", forHTTPHeaderField: "Content-Type")
        request.addValue(model.accessToken ?? "", forHTTPHeaderField: "access-token")
        request.addValue(model.client ?? "", forHTTPHeaderField: "client")
        request.addValue(model.uid ?? "", forHTTPHeaderField: "uid")
        
        return request
    }
    
    func setModel(data: Data?) {
        enterprisesModel = try? JSONDecoder().decode(EnterprisesModel.self, from: data!)
    }
}
