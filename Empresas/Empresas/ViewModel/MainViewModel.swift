//
//  MainViewModel.swift
//  Empresas
//
//  Created by Marcos Jr on 30/05/21.
//

import UIKit

class MainViewModel: MainDelegate {

    var model: LoginModel!
    var headersModel: HeadersModel! {
        didSet {
            searchViewModel.model = self.headersModel
        }
    }
    var searchViewModel: SearchViewModel!
    let endpoint = Endpoint()
    
    init(viewController: MainViewController) {
        self.model = LoginModel()
        headersModel = viewController.headersModel
        self.searchViewModel = SearchViewModel(model: headersModel)
        viewController.delegate = self
    }
    
    func setEmail(_ email: String) {
        model.email = email
    }
    
    func setPassword(_ password: String) {
        model.password = password
    }

    func signIn(completionHandler: @escaping (HTTPURLResponse) -> Void) {
        guard let url = URL(string: endpoint.login) else { return }
        var request = URLRequest(url: url)
        
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        do {
            let params = model.dictionaryValue
            request.httpBody = try JSONSerialization.data(withJSONObject: params ?? "", options: .init())
            
            URLSession.shared.dataTask(with: request) { (data, response, error) in
                if let error = error {
                    print("Failed to login: ", error)
                    return
                }
                guard let httpResponse = response as? HTTPURLResponse else { return }
                
                self.headersModel.contentType = httpResponse.value(forHTTPHeaderField: "Content-Type")
                self.headersModel.accessToken = httpResponse.value(forHTTPHeaderField: "access-token")
                self.headersModel.client = httpResponse.value(forHTTPHeaderField: "client")
                self.headersModel.uid = httpResponse.value(forHTTPHeaderField: "uid")
                
                completionHandler(httpResponse)
            }.resume()
        } catch {
            print("Failed to serialize data: ", error)
        }
    }
}
