//
//  Extensions.swift
//  Empresas
//
//  Created by Marcos Jr on 30/05/21.
//

import UIKit

// MARK: - Encodable Extension
extension Encodable {
    var dictionaryValue: [String: Any]? {
        let encoder = JSONEncoder()
        guard let data = try? encoder.encode(self) else {
            return nil
        }
        
        do {
            let jsonDictionary = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
            return jsonDictionary as? [String: Any]
        } catch {
            return nil
        }
    }
}

// MARK: - UIImageView

extension UIImageView {
    func load(url: URL) {
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self?.image = image
                    }
                }
            }
        }
    }
}

// MARK: - UITableView Extension
extension UITableView {

    func register(_ cell: UITableViewCell.Type) {
        let nib = UINib(nibName: cell.identifier, bundle: nil)
        register(nib, forCellReuseIdentifier: cell.identifier)
    }
    
    func dequeueReusableCell<T: UITableViewCell>(of class: T.Type,
                                                 for indexPath: IndexPath,
                                                 configure: @escaping ((T) -> Void) = { _ in }) -> UITableViewCell {
        let cell = dequeueReusableCell(withIdentifier: T.identifierItem, for: indexPath)
        if let typedCell = cell as? T {
            configure(typedCell)
        }
        return cell
    }
}

// MARK: - UIView Extension
extension UIView {
    
    public static var identifier: String {
        return String(describing: self)
    }
    
    public static func fromNib<T: UIView>() -> T {
        guard let result = Bundle.main.loadNibNamed(T.identifier, owner: nil, options: nil)?.first as? T else {
            fatalError("Error in dequeing cell: \(T.identifier)")
        }
        
        return result
    }
    
    func shake() {
        let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        animation.duration = 0.6
        animation.values = [-20.0, 20.0, -20.0, 20.0, -10.0, 10.0, -5.0, 5.0, 0.0 ]
        layer.add(animation, forKey: "shake")
    }
    
    static var identifierItem: String { return String(describing: self) }
    
    static func fromNib<T: UIView>(owner: Any? = nil) -> T {
        guard let result = Bundle.main.loadNibNamed(T.identifierItem, owner: owner, options: nil)?.first as? T else {
            fatalError(": \(T.identifierItem)")
        }
        return result
    }
}
