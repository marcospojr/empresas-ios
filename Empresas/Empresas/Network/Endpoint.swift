//
//  Endpoint.swift
//  Empresas
//
//  Created by Marcos Jr on 30/05/21.
//

import UIKit

import Foundation

public class Endpoint {
    
    static let baseUrl = "https://empresas.ioasys.com.br"
    static let api_version = "v1"
       
    let login: String               = "\(baseUrl)/api/\(api_version)/users/auth/sign_in"
    let getCompanies: String        = "\(baseUrl)/api/\(api_version)/enterprises?name="
}
