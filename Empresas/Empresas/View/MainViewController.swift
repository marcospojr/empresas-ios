//
//  ViewController.swift
//  Empresas
//
//  Created by Marcos Jr on 29/05/21.
//

import UIKit

protocol MainDelegate: AnyObject {
    func signIn(completionHandler: @escaping (HTTPURLResponse) -> Void)
    func setEmail(_ email: String)
    func setPassword(_ password: String)
}

class MainViewController: UIViewController {

    @IBOutlet weak var welcomeLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordLabel: UILabel!
    @IBOutlet weak var passwordTextField : UITextField!
    @IBOutlet weak var loginButton: UIButton!
    
    var searchViewController: SearchViewController!
    let myActivityIndicator = UIActivityIndicatorView()
    let eyeButton = UIButton(type: .custom)
    var headersModel: HeadersModel!
    var viewModel: MainViewModel!
    weak var delegate: MainDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        headersModel = HeadersModel()
        self.viewModel = MainViewModel(viewController: self)
        setupSearchView()
        setupEyeButton()
        emailTextField.addTarget(self, action: #selector(setEmail(_:)), for: .allEditingEvents)
        passwordTextField.addTarget(self, action: #selector(setPassword(_:)), for: .allEditingEvents)
     }
    
    func setupSearchView() {
                
        searchViewController = storyboard?.instantiateViewController(identifier: "SearchViewController", creator: { coder in
            return SearchViewController(coder: coder, viewModel: self.viewModel.searchViewModel)
        })
        searchViewController.modalPresentationStyle = .fullScreen
        searchViewController.modalTransitionStyle = .crossDissolve
        viewModel.searchViewModel = searchViewController.viewModel
    }
    
    func setupEyeButton() {
        eyeButton.setImage(UIImage(systemName: "eye.fill"), for: .normal)
        eyeButton.tintColor = .gray
        eyeButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: -24, bottom: 0, right: 15)
        eyeButton.addTarget(self, action: #selector(self.buttonPasswordVisiblity), for: .touchUpInside)
        
        passwordTextField.rightView = eyeButton
        passwordTextField.rightViewMode = .always
    }
    
    @IBAction func login(_ sender: Any) {
        myActivityIndicator.center = view.center
        myActivityIndicator.hidesWhenStopped = false
        myActivityIndicator.startAnimating()
         
        view.addSubview(myActivityIndicator)
        
        delegate?.signIn(completionHandler: validateResponse)
    }

    @IBAction func buttonPasswordVisiblity(_ sender: Any) {
         (sender as! UIButton).isSelected = !(sender as! UIButton).isSelected

         if (sender as! UIButton).isSelected {
             self.passwordTextField.isSecureTextEntry = false
             eyeButton.setImage(UIImage(systemName: "eye.slash.fill"), for: .normal)
         } else {
             self.passwordTextField.isSecureTextEntry = true
             eyeButton.setImage(UIImage(systemName: "eye.fill"), for: .normal)
         }
    }
    
    @objc func setEmail(_ sender: UITextField) {
        delegate?.setEmail(sender.text ?? "")
    }
    
    @objc func setPassword(_ sender: UITextField) {
        delegate?.setPassword(sender.text ?? "")
    }
    
    @objc func setupTextFieldsError(to isErrored: Bool) {
        DispatchQueue.main.async {
            if isErrored {
                self.emailLabel.textColor = .red
                self.emailTextField.textColor = .red
                self.passwordLabel.textColor = .red
                self.passwordTextField.textColor = .red
            } else {
                self.emailLabel.textColor = .gray
                self.emailTextField.textColor = .gray
                self.passwordLabel.textColor = .gray
                self.passwordTextField.textColor = .gray
            }
        }
    }
    
    func validateResponse(response: HTTPURLResponse) {
        DispatchQueue.main.async {
            self.removeActivityIndicator(activityIndicator: self.myActivityIndicator)

            switch response.statusCode {
            case 200:
                self.setupTextFieldsError(to: false)
                self.present(self.searchViewController, animated: true, completion: .none)
            case 401:
                self.setupTextFieldsError(to: true)
                AlertManager(with: .error, title: AlertTitles.invalidCredentials,
                             message: AlertMessages.loginError).show()
                print(response.debugDescription)

            default:
                self.setupTextFieldsError(to: true)
                AlertManager(with: .error, title: AlertTitles.networkError,
                             message: AlertMessages.cannotConnect).show()
                print(response.debugDescription)
            }
        }
    }
    
    func removeActivityIndicator(activityIndicator: UIActivityIndicatorView) {
        DispatchQueue.main.async {
            activityIndicator.stopAnimating()
            activityIndicator.removeFromSuperview()
        }
    }
}
