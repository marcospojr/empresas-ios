//
//  SearchViewController.swift
//  Empresas
//
//  Created by Marcos Jr on 30/05/21.
//

import UIKit

protocol SearchDelegate: AnyObject {
    func getCompanies(param: String, completionHandler: @escaping (HTTPURLResponse, Data?) -> Void)
    func setModel(data: Data?)
}

class SearchViewController: UIViewController {
    
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var resultLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!

    var viewModel: SearchViewModel!
    weak var delegate: SearchDelegate?
    var enterprisesModel: EnterprisesModel!
    
    var resultNumber: Int = 0 {
        didSet {
            if resultNumber == 0 {
                resultLabel.text = "Nenhum resultado encontrado"
            } else {
                resultLabel.text = "\(resultNumber) resultados encontrados."
            }
        }
    }
    
    let findButton = UIButton(type: .custom)
    
    init?(coder: NSCoder, viewModel: SearchViewModel) {
        self.viewModel = viewModel
        viewModel.enterprisesModel = enterprisesModel
        super.init(coder: coder)
    }
    
    required init?(coder: NSCoder) {
        fatalError("You must create this view controller with a view model.")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
                
        setupFindButton()
        self.delegate = viewModel
        self.searchTextField.addTarget(self, action: #selector(searchCompanies(_:)), for: .allEditingEvents)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupTableView()
    }
    
    func setupFindButton() {
        findButton.setImage(UIImage(systemName: "magnifyingglass"), for: .normal)
        findButton.tintColor = .gray
        findButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: -10)
        findButton.isUserInteractionEnabled = false
        
        searchTextField.leftView = findButton
        searchTextField.leftViewMode = .always
    }
    
    @objc func searchCompanies(_ sender: UITextField) {
        delegate?.getCompanies(param: sender.text ?? "", completionHandler: validateResponse)
    }
    
    func validateResponse(response: HTTPURLResponse, data: Data?) {
        DispatchQueue.main.async {
            if response.statusCode == 200 {
                self.delegate?.setModel(data: data)
                self.tableView.reloadData()
            } else {
                AlertManager(with: .error, title: AlertTitles.networkError,
                             message: AlertMessages.cannotConnect).show()
                print(response.debugDescription)
            }
        }
    }
}

extension SearchViewController: UITableViewDelegate, UITableViewDataSource {
    
    func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self

        tableView.register(CompanyTableViewCell.self)
        viewModel.tableView = tableView
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return tableView.dequeueReusableCell(of: CompanyTableViewCell.self, for: indexPath) { (cell) in
            cell.model = self.viewModel.enterprisesModel.enterprises?[indexPath.row]
            cell.viewController = self
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        resultNumber = (viewModel.enterprisesModel?.enterprises?.count ?? 0)
        return resultNumber
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let temp = UIView()
        temp.backgroundColor = #colorLiteral(red: 0.9725490196, green: 0.9725490196, blue: 0.9725490196, alpha: 1)
        return temp
    }
}

