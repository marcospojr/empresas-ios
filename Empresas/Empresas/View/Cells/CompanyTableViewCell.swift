//
//  CompanyTableViewCell.swift
//  Empresas
//
//  Created by Marcos Jr on 30/05/21.
//

import UIKit

class CompanyTableViewCell: UITableViewCell {
    
    @IBOutlet weak var view: UIView!
    @IBOutlet weak var companyImageView: UIImageView!
    @IBOutlet weak var companyLabel: UILabel!
    
    var model: Enterprises! {
        didSet {
            setCompanyDetail()
        }
    }
    var viewController: UIViewController!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        configure()
        setCompanyDetail()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()

        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 4, left: 0, bottom: 4, right: 0))
    }
    
    func configure() {
        // View cornerRadius
        view.layer.cornerRadius = 4;
        view.layer.masksToBounds = true;
        
        // Gesture Recognizer
        let tapGesture = UITapGestureRecognizer()
        self.view.addGestureRecognizer(tapGesture)
        tapGesture.addTarget(self, action: #selector(onTap(_:)))
    }
    
    func setCompanyDetail() {
        guard let imageUrl = URL(string: Endpoint.baseUrl + (model?.photo ?? "")) else { return }
        companyImageView.load(url: imageUrl)
        companyLabel.text = model?.enterpriseName ?? ""
    }
    
    @objc func onTap(_ sender: UITapGestureRecognizer) {
        guard let detailViewController = viewController.storyboard?.instantiateViewController(identifier: "DetailViewController", creator: { coder in
            return DetailViewController(model: self.model, coder: coder)
        }) else { return }
        
        viewController.present(detailViewController, animated: true, completion: .none)
    }
}
