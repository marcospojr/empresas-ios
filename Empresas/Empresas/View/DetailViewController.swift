//
//  DetailViewController.swift
//  Empresas
//
//  Created by Marcos Jr on 01/06/21.
//

import UIKit

class DetailViewController: UIViewController {
    
    @IBOutlet weak var detailTitle: UINavigationItem!
    @IBOutlet weak var detailImage: UIImageView!
    @IBOutlet weak var detailText: UITextView!
    
    var model: Enterprises! {
        didSet {
            setCompanyDetail()
        }
    }
    
    init?(model: Enterprises, coder: NSCoder) {
        super.init(coder: coder)
        self.model = model
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setCompanyDetail()
    }
    
    func setCompanyDetail() {
        guard let imageUrl = URL(string: Endpoint.baseUrl + (model?.photo ?? "")) else { return }
        detailImage.load(url: imageUrl)
        detailTitle.title = model?.enterpriseName ?? ""
        detailText.text = model?.enterpriseDescription ?? ""
    }
}
