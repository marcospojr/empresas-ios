//
//  HeadersModel.swift
//  Empresas
//
//  Created by Marcos Jr on 31/05/21.
//

import UIKit

struct HeadersModel: Codable {
    var contentType: String?
    var accessToken: String?
    var client: String?
    var uid: String?
}
