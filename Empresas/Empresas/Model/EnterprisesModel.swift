//
//  EnterprisesModel.swift
//  Empresas
//
//  Created by Marcos Jr on 31/05/21.
//

import Foundation

// MARK: - EnterprisesModel
struct EnterprisesModel: Codable {
    let enterprises: [Enterprises]?
}

// MARK: - Enterprises
struct Enterprises: Codable {
    let id: Int
    let emailEnterprise, facebook, twitter, linkedin: String?
    let phone: String?
    let ownEnterprise: Bool?
    let enterpriseName, photo, enterpriseDescription, city: String?
    let country: String?
    let value, sharePrice: Int?
    let enterpriseType: EnterpriseType

    enum CodingKeys: String, CodingKey {
        case id
        case emailEnterprise = "email_enterprise"
        case facebook, twitter, linkedin, phone
        case ownEnterprise = "own_enterprise"
        case enterpriseName = "enterprise_name"
        case photo
        case enterpriseDescription = "description"
        case city, country, value
        case sharePrice = "share_price"
        case enterpriseType = "enterprise_type"
    }
}

// MARK: - EnterpriseType
struct EnterpriseType: Codable {
    let id: Int
    let enterpriseTypeName: String

    enum CodingKeys: String, CodingKey {
        case id
        case enterpriseTypeName = "enterprise_type_name"
    }
}
