//
//  LoginModel.swift
//  Empresas
//
//  Created by Marcos Jr on 30/05/21.
//

import UIKit

struct LoginModel: Codable {
    var email: String?
    var password: String?
}
